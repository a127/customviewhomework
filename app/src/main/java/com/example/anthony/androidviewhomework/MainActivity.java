package com.example.anthony.androidviewhomework;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity
{
    private MyView mv;

    private float x_touch = 0;
    private float y_touch = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        mv = new MyView(this);
        mv.setBackgroundColor(Color.WHITE);

        setContentView(mv);

        mv.setOnTouchListener(new MyTouchListener());
    }

    private class MyTouchListener implements OnTouchListener
    {

        boolean flag = false;
        public boolean onTouch(View v, MotionEvent event)
        {
            if(event.getAction() == MotionEvent.ACTION_DOWN)
            {
                x_touch = event.getX();
                y_touch = event.getY();

                flag = true;

                mv.setTouchCoordinates(x_touch, y_touch);
                mv.invalidate();

            }
            return flag;
        }
    }


}

