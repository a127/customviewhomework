package com.example.anthony.androidviewhomework;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

/**
 * Created by Anthony on 3/10/2015.
 */
public class MyView extends View
{
    private float width;
    private float height;
    private Paint p;

    private float x_coor;
    private float y_coor;

    public MyView(Context context)
    {
        super(context);



        Log.i("Width => ", width + "");
        Log.i("Height =>", height + "");

        p = new Paint();

        p.setColor(Color.RED);

    }

    public void setTouchCoordinates(float x, float y)
    {
        x_coor = x;
        y_coor = y;

    }

    public void onDraw(Canvas c)
    {
        width   = getMeasuredWidth();
        height  = getMeasuredHeight();

        p.setColor(Color.RED);

        c.drawRect(0, 0, 100, 100, p);

        p.setColor(Color.BLUE);

        c.drawRect(width - 100, height - 100, width, height, p);

        p.setColor(Color.GREEN);

        c.drawCircle(width/2, height/2, 100, p);

        p.setColor(Color.BLACK);
        p.setTextSize(100);
        c.drawText("X Coordinate => " + x_coor, 100, 100, p);
        c.drawText("Y Coordinate => " + y_coor, 100, 200, p);


    }

}
